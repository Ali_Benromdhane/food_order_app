import React, { Fragment } from "react";
import Header from "./components/Layout/Header";
import "./components/Layout/Header.module.css";
function App() {
  return (
    <Fragment>
      <Header />
    </Fragment>
  );
}

export default App;

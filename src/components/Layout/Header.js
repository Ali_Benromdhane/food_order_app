import React, { Fragment } from "react";
import mealsImage from "../../assests/images/mealsImage.jpg";
import classes from "./Header.module.css";

const Header = () => {
  return (
    <Fragment>
      <header className={classes.header}>
        <h1>Order App</h1>
        <button>Cart</button>
      </header>
      <div className={classes["header__main-image"]}></div>
    </Fragment>
  );
};

export default Header;
